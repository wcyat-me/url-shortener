require("dotenv").config();
const { MongoClient } = require("mongodb");
const express = require("express");
const mongouri = process.env.DB_URI;
const app = express();
app.get("/", (req, res) => {
  res.set("location", "https://us.wcyat.me");
  res.status(301).send();
});
app.get("/:id", async (req, res) => {
  const client = new MongoClient(mongouri);
  if (typeof req.params.id !== "string" || req.params.id.length > 6)
    return res.status(400).send("Bad request.");
  
  try {
    await client.connect();
    const slinksCl = client.db("slinks").collection("slinks");
    const slink = await slinksCl.findOne({ id: req.params.id });

    if (!slink) return res.status(404).send("Not found");

    if (slink.encrypted)
      return res.redirect(301, `https://us.wcyat.me/${req.params.id}`);

    return res.redirect(301, slink.url);
  } finally {
    await client.close();
  }
});
app.listen(5001, () => {
  console.log("Listening at port 5001.");
});

const express = require("express");
const cors = require("cors");
const body_parser = require("body-parser");
const { MongoClient } = require("mongodb");
const rg = require("wcyat-rg");
const dotenv = require("dotenv");
const isUrlHttp = require("is-url-http");
dotenv.config();
const uri = process.env.DB_URI;
const app = express();
app.use(cors());
app.options("*", cors());
app.get("/:id", async (req, res) => {
  if (typeof req.params.id !== "string" || req.params.id.length > 6) {
    res.status(400);
    res.send("Bad request.");
    return;
  }
  const client = new MongoClient(uri);
  try {
    await client.connect();
    const slinks = client.db("slinks").collection("slinks");
    const o = await slinks.findOne(
      { id: req.params.id },
      { projection: { _id: 0, password: 0 } }
    );
    if (!o) {
      res.status(404);
      res.send("Not found.");
      return;
    }
    if (o.encrypted) delete o.url;
    res.send(o);
  } finally {
    await client.close();
  }
});
app.post("/create", body_parser.json(), async (req, res) => {
  if (
    !req.body.url ||
    (Object.keys(req.body).length !== 3 && !isUrlHttp(req.body.url)) ||
    (Object.keys(req.body).length !== 1 &&
      Object.keys(req.body).length !== 3) ||
    (Object.keys(req.body).length === 3 &&
      (!req.body.encrypted || !req.body.password)) ||
    !(
      typeof req.body.url === "string" &&
      (req.body.encrypted
        ? typeof req.body.encrypted === "boolean" &&
          typeof req.body.password === "string"
        : 1)
    ) ||
    !(() => {
      if (req.body.encrypted) return true;
      try {
        new URL(req.body.url);
        return true;
      } catch (e) {
        return false;
      }
    })()
  ) {
    res.status("400");
    res.send("Bad request");
    return;
  }
  const client = new MongoClient(uri);
  try {
    await client.connect();
    const slinks = client.db("slinks").collection("slinks");
    const r = req.body.encrypted
      ? undefined
      : await slinks.findOne({ url: req.body.url });
    let id = r
      ? r.id
      : rg.generate({
          include: { numbers: true, upper: true, lower: true, special: false },
          digits: 6,
        });
    if (!r) {
      while (await slinks.find({ id: id }).count()) {
        id = rg.generate({
          include: { numbers: true, upper: true, lower: true, special: false },
          digits: 6,
        });
      }
      await (!req.body.encrypted
        ? slinks.insertOne({ id: id, url: req.body.url })
        : slinks.insertOne({
            id: id,
            url: req.body.url,
            password: req.body.password,
            encrypted: true,
          }));
    }
    res.send({ id: id });
  } finally {
    await client.close();
  }
});
app.post("/check", body_parser.json(), async (req, res) => {
  if (
    !req.body.password ||
    !req.body.id ||
    Object.keys(req.body).length !== 2
  ) {
    res.status(400);
    res.send("Bad request");
    return;
  }
  const client = new MongoClient(uri);
  try {
    await client.connect();
    const slinks = client.db("slinks").collection("slinks");
    const o = await slinks.findOne({ id: req.body.id });
    if (!o) {
      res.status(404);
      res.send("Not found.");
      return;
    }
    if (o.password !== req.body.password) {
      res.status(401);
      res.send("Password incorrect.");
      return;
    }
    res.send({ url: o.url, id: o.id });
  } finally {
    await client.close();
  }
});
app.listen(5000, () => {
  console.log("Listening at port 5000.");
});

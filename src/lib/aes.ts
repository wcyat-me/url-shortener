import CryptoJS from "crypto-js";
export function encrypt(text = "", key = "") {
  const encryptedmessage = CryptoJS.AES.encrypt(text, key);
  return encryptedmessage.toString();
}
export function decrypt(text = "", key = "") {
  const code = CryptoJS.AES.decrypt(text, key);
  const decryptedMessage = code.toString(CryptoJS.enc.Utf8);
  return decryptedMessage;
}
